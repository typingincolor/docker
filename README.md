# Docker files

# queryapi
    docker run -p 8080:8080  --link redis:db -d --env-file ./config/queryapi.cfg -t typingincolor/queryapi

## queryapi.cfg
    url=http://sandbox.api.laterooms.com
    tlrgAppId=xxxxxx
    cacheType=redis

# redis
    docker run -p 6379:6379 --name redis -d typingincolor/redis 


# port forwarding (OSX)
    VBoxManage controlvm boot2docker-vm natpf1 "name,tcp,127.0.0.1,8080,,8080"
